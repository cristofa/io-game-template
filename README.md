### IO game template

This a template for Server-Authoritative IO game. It uses headless Phaser
game running in JSDOM to manager physics and Phaser client to gather player's input.

This template implements following features:
* node server with Hot Module Replacement
* HTML overlay written in React/Redux
* webpack dev server with Hot Module Replacement
* webpack configuration for development and production build

Game client and React overlay communicate using Redux reducers.
