import React from 'react';
import loadable from '@loadable/component';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';

const Overlay = loadable(() => import('../components/Overlay'));
import store from './store';
import Game from '../components/Game';

const App = () =>
  <Provider store={store}>
    <Game/>
    <Overlay/>
  </Provider>;

export default hot(App);
