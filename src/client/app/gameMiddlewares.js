import { createDynamicMiddlewares } from 'redux-dynamic-middlewares';

const gameMiddlewares = createDynamicMiddlewares();

export default gameMiddlewares;
