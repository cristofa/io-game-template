import { createSlice } from '@reduxjs/toolkit';

const initialState ={
  uiVisible: true,
};

export const gameSlice = createSlice({
  name: 'players',
  initialState,
  reducers: {
    setUIVisible: (state, action) => {
      state.uiVisible = action.payload;
    },
  },
});

export const { setUIVisible } = gameSlice.actions;


export default gameSlice.reducer;
