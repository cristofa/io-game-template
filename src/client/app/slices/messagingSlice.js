import { createSlice } from '@reduxjs/toolkit';

const initialState = { host: '', connected: null,  error: '' };


const messaging = createSlice({
  name: 'messaging',
  initialState,
  reducers: {
    playGame: () => {
    },
  }
});

export const {
  playGame
} = messaging.actions;


export default messaging.reducer;
