import { createSlice, createEntityAdapter } from '@reduxjs/toolkit';

const playersAdapter = createEntityAdapter({
  selectId: (player) => player.playerId
});

const initialState = playersAdapter.getInitialState({
  ids: [],
  entities: {
  },
});

export const playersSlice = createSlice({
  name: 'players',
  initialState,
  reducers: {
    updatePlayers: (state, action) => {
      playersAdapter.setAll(state, action.payload.players);
    },
  },
});

export const { updatePlayers } = playersSlice.actions;

export const {
  selectAll: selectAllPlayers,
  selectById: selectPlayerById,
  selectIds: selectPlayersIds,
} = playersAdapter.getSelectors(state => state.players);

export default playersSlice.reducer;
