import React from 'react';
import ReactDOM from 'react-dom';
import {ThemeProvider} from '@material-ui/core/styles';
import {loadableReady} from '@loadable/component';
import {BrowserRouter} from 'react-router-dom';

import App from './app/App';
import theme from '../common/theme';


function Main() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <App/>
      </ThemeProvider>
    </BrowserRouter>
  );
}

loadableReady(() => {
  ReactDOM.render(<Main/>, document.querySelector('#root'));
});
