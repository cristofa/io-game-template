import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Phaser from 'phaser';
import {useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Helmet} from 'react-helmet';

import {setUIVisible} from '../app/slices/gameSlice';
import gameConfig from '../game/gameConfig';
import commonConfig from '../../common/commonConfig';
import gameMiddlewares from '../app/gameMiddlewares';


const useStyles = makeStyles(() => ({
  game: {
    overflow: 'hidden',
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    top: 0,
    left: 0
  }
}));

const Game = () => {
  const classes = useStyles();
  const dispatch = useDispatch();


  useEffect(() => {
    const game = new Phaser.Game(gameConfig);
    game.overlay = bindActionCreators({setUIVisible}, dispatch);
    game.middlewares = gameMiddlewares;
  }, []);

  return (
    <React.Fragment>
      <Helmet>
        <script src={`http://${commonConfig.host}:${3002}/socket.io/socket.io.js`}></script>
      </Helmet>
      <div id='game' className={classes.game}></div>
    </React.Fragment>);
};

export default Game;
