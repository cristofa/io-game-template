import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {useSelector} from 'react-redux';

import PlayButton from './PlayButton';

const useStyles = makeStyles(() => ({
  overlay: {
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    fontFamily: 'Roboto',
    top: 0,
    left: 0
  }
}));

const Overlay = () => {

  const classes = useStyles();
  const visible = useSelector(state => state.game.uiVisible);

  return (
    <div className={classes.overlay} style={visible ? {} : {visibility: 'hidden'}}>
      <PlayButton/>
    </div>);
};

export default Overlay;
