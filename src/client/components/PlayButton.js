import React from 'react';
import {useDispatch} from 'react-redux';
import {Button} from '@material-ui/core';

import { playGame } from '../app/slices/messagingSlice';


const Overlay = () => {

  const dispatch = useDispatch();

  const playCallback = () => {
    dispatch(playGame());
  };

  return (
    <Button variant='contained' color='primary' onClick={playCallback}>
      Play as Guest
    </Button>);
};

export default Overlay;
