import Phaser from 'phaser';

import Preload from './scenes/Preload';
import Boot from './scenes/Boot';
import Lobby from './scenes/Lobby';
import Play from './scenes/Play';

const config = {
  type: Phaser.WEBGL,
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'game',
    width: 1280,
    height: 720,
  },
  scene: [Boot, Preload, Lobby, Play ],
  transparent: true
};

export default config;



