import Phaser from 'phaser';

class ClientPlayer extends Phaser.GameObjects.Container {
  _points = 0;
  _playerId;

  constructor(scene, x, y, team, playerId) {
    super(scene, x, y);
    this.sprite = scene.add.sprite(0, 0, 'player');
    this.sprite.setOrigin(0.5, 0.5).setDisplaySize(50, 50);

    if (team === 'blue') {
      this.sprite.setTint(0x0000ff);
    } else {
      this.sprite.setTint(0xff0000);
    }
    this._playerId = playerId;

    this.pointsText = scene.add.text(0, -45, this._points, { fontFamily: 'Georgia, Times, serif' });
    this.add(this.sprite);
    this.add(this.pointsText);
    scene.add.existing(this);

  }

  setAngle(angle) {
    this.sprite.setRotation(angle);
  }

  get playerId() {
    return this._playerId;
  }

  set points(value) {
    this.pointsText.text = value;
    this._points = value;
  }

}

export default ClientPlayer;
