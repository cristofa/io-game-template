
class ClientSocket  {
  constructor() {
    const server = 'http://localhost:3002';
    const connectionOptions = {
      'force new connection': true,
      'reconnectionAttempts': 'Infinity',
      'timeout': 10000,
      'transports': ['websocket']
    };

    // eslint-disable-next-line no-undef
    this.socket = io(server, connectionOptions);
  }

  emit(event, args) {
    this.socket.emit(event, args);
  }

  init(handlers) {

    Object.keys(handlers).forEach(h => {
      this.socket.on(h, (args) => {
        handlers[h](args);
      });
    });

  }

}


export default ClientSocket;
