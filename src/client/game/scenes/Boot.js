import Phaser from 'phaser';

class Boot extends Phaser.Scene {
  constructor() {
    super({key: 'Boot'});
  }

  preload() {
    this.load.image('logo', 'assets/images/logo.png');
  }

  create() {
    this.scene.start('Preload');
  }

}

export default Boot;
