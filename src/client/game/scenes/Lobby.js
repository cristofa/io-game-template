import Phaser from 'phaser';

class Preload extends Phaser.Scene {
  constructor() {
    super({key: 'Lobby'});
  }

  preload() {
  }

  create() {

    const middleware = () => next => (action) => {
      switch (action.type) {
      case 'messaging/playGame':
        this.game.middlewares.removeMiddleware(middleware);
        this.scene.switch('Play');
        break;
      default:
        return next(action);
      }
    };
    this.game.middlewares.addMiddleware(middleware);

    this.cameras.main.backgroundColor.setFromRGB({r: 30,g: 30, b: 30,a: 150});

    let graphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00 }, fillStyle: { color: 0xff0000 }});
    for (let i = 0; i < 10; i++) {
      let circle = new Phaser.Geom.Circle(Math.random()*1000, Math.random()*800, Math.random()*250);
      graphics.strokeCircleShape(circle);
    }
    this.game.overlay.setUIVisible(true);
  }
}

export default Preload;
