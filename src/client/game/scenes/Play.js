import Phaser from 'phaser';

import ClientSocket from '../protocol/ClientSocket';
import ClientPlayer from '../model/ClientPlayer';
import NET_MSGS from '../../../common/NET_MSGS';

class Play extends Phaser.Scene {

  constructor() {
    super({key: 'Play' });
  }

  removePlayer(playerId) {
    this.players.getChildren().forEach((player) => {
      if (playerId === player.playerId) {
        player.destroy();
      }
    });
  }

  displayPlayer(playerInfo) {
    const player = new ClientPlayer(this, playerInfo.x, playerInfo.y, playerInfo.team, playerInfo.playerId);
    this.players.add(player);
  }

  initPlayers(players) {
    Object.keys(players).forEach((id) => {
      this.displayPlayer(players[id]);
    });
  }

  updatePlayers(players) {
    this.players.getChildren().forEach((player) => {
      const info = players[player.playerId];
      if (info) {
        player.setAngle(info.rotation);
        player.setPosition(info.x, info.y);
        player.points = info.points;
      }
    });
  }

  create() {
    this.cameras.main.backgroundColor.setFromRGB({r: 30,g: 30, b: 30,a: 150});
    let graphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00 }, fillStyle: { color: 0xff0000 }});
    let circle = new Phaser.Geom.Circle(400, 300, 200);
    graphics.strokeCircleShape(circle);

    this.players = this.add.group();

    this.socket = new ClientSocket();
    this.socket.init(this.createHandlers());

    this.cursors = this.input.keyboard.createCursorKeys();
    this.leftKeyPressed = false;
    this.rightKeyPressed = false;
    this.upKeyPressed = false;
    this.spacePressed = false;

    this.keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
  }

  createHandlers() {
    const handlers = [];
    handlers[NET_MSGS.DISCONNECT] = this.removePlayer.bind(this);
    handlers[NET_MSGS.PLAYER.INIT] = this.initPlayers.bind(this);
    handlers[NET_MSGS.PLAYER.UPDATE] = this.updatePlayers.bind(this);
    handlers[NET_MSGS.PLAYER.NEW] = this.displayPlayer.bind(this);
    return handlers;
  }

  update() {

    const left = this.leftKeyPressed;
    const right = this.rightKeyPressed;
    const up = this.upKeyPressed;
    const space = this.spacePressed;

    if (this.cursors.left.isDown) {
      this.leftKeyPressed = true;
    } else if (this.cursors.right.isDown) {
      this.rightKeyPressed = true;
    } else {
      this.leftKeyPressed = false;
      this.rightKeyPressed = false;
    }

    this.upKeyPressed = this.cursors.up.isDown;
    this.spacePressed = Phaser.Input.Keyboard.JustDown(this.keySpace);

    if (left !== this.leftKeyPressed || right !== this.rightKeyPressed ||
      up !== this.upKeyPressed || space !== this.spacePressed) {

      this.socket.emit(NET_MSGS.PLAYER.INPUT, { left: this.leftKeyPressed , right: this.rightKeyPressed,
        up: this.upKeyPressed, space: this.spacePressed });
    }

  }

}

export default Play;
