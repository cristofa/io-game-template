import Phaser from 'phaser';

import ProgressBar from '../utils/ProgressBar';


class Preload extends Phaser.Scene {
  constructor() {
    super({key: 'Preload'});
  }

  preload() {
    const progressBar = new ProgressBar(this, this.cameras.main.width/2, this.cameras.main.height/2);
    this.add.existing(progressBar);

    this.cameras.main.backgroundColor.setFromRGB({r: 0,g: 0, b: 0,a: 255});

    //load all assets here
    this.load.on('progress', (value) => {
      console.log('Progress: ' + value);
      progressBar.progress(value);

    });

    this.load.on('complete', () => {
      this.scene.switch('Lobby');
    });
    this.load.image('player', 'assets/images/player1.png');

  }
}

export default Preload;
