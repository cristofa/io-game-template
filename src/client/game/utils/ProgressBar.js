import Phaser from 'phaser';

class ProgressBar extends Phaser.GameObjects.Container {

  constructor(scene, x, y) {
    super(scene, x, y);

    this.barLenght = 500;
    this.barHeight = 30;
    this.margin = 10;


    this.progressBar = new Phaser.GameObjects.Graphics(this.scene);
    const progressBox = new Phaser.GameObjects.Graphics(this.scene);
    this.add(this.progressBar);
    this.add(progressBox);

    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(-this.barLenght/2, 0, this.barLenght + this.margin * 2, this.barHeight + this.margin * 2);

    const logo = this.scene.add.image(this.margin, -20, 'logo').setOrigin(0.5, 1).setScale(0.4, 0.4);
    this.add(logo);
  }


  progress(percent) {
    this.progressBar.clear();
    this.progressBar.fillStyle(0xffffff, 1);
    this.progressBar.fillRect(this.margin -this.barLenght/2, this.margin, this.barLenght * percent, this.barHeight);
  }

}

export default ProgressBar;
