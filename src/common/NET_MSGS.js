const NET_MSGS = {
  DISCONNECT: 'disconnected',
  PLAYER: {
    UPDATE: 'playerUpdates',
    NEW: 'newPlayer',
    INIT: 'initPlayers',
    INPUT: 'playerInput',
    SET_NAME: 'setName'
  },
};

export default NET_MSGS;
