export default {
  port: process.env.PORT || 3000,
  wsPort: process.env.WS_PORT || 3002,
  host: process.env.HOST || 'localhost'
};
