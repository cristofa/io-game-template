const CONST = {
  overlayEvent: 'msgToOverlay',
  gameEvent: 'msgToGame'
};

export default CONST;
