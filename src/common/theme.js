import { createMuiTheme } from '@material-ui/core/styles';

let theme = createMuiTheme({
  palette: {
    background: {
      paper: '#e0e0e0',
      default: '#f5f5f5',
    },
    primary: {
      main: '#76ff03',
      contrastText: '#000000',
    },
    secondary: {
      main: '#2196f3',
      contrastText: '#000000',
    },
    error: {
      main: '#f34834',
      contrastText: '#000',
    },
    text: {
      primary: '#000',
      secondary: '#000',
    },
  }
});

export default theme;
