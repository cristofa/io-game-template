export default {
  sessionSecret: process.env.SESSION_SECRET || 'sessionsecret',
  dbName: process.env.DB_NAME || 'karin',
  dbUser: process.env.DB_NAME || 'karin',
  dbPass: process.env.DB_NAME || 'karin',
  dbHost: process.env.DB_HOST || 'localhost',
  dbPort: process.env.DB_PORT || '5432',
  discordClientId: process.env.DISCORD_CLIENT_ID,
  discordSecret: process.env.DISCORD_SECRET

};
