import express from 'express';
import passport from 'passport';
import Strategy from 'passport-discord';

import config from '../config';
import {checkAuth} from '../utils';

class AuthCtrl {
  _controller = express.Router();

  constructor() {
    passport.serializeUser((user, done) => {
      done(null, user);
    });
    passport.deserializeUser((obj, done) => {
      done(null, obj);
    });

    const tokenToProfile = async (accessToken, refreshToken, profile, done) => {
      done(null, profile);
    };

    const strategy = new Strategy({
      clientID: config.discordClientId,
      clientSecret: config.discordSecret,
      callbackURL: 'http://localhost:3000/login',
      scope: ['identify'],
      prompt: 'consent'
    }, tokenToProfile);

    passport.use(strategy);

    this._controller.get('/login', passport.authenticate('discord', {
      session: true,
      successReturnToOrRedirect: '/test'
    }));

    this._controller.get('/logout', (req, res) => {
      req.logout();
      res.redirect('/');
    });

    this._controller.get('/test', checkAuth, (req, res) => {
      res.send('logged in ' + JSON.stringify(req.session));
    });
  }


  get controller() {
    return this._controller;
  }
}

export default AuthCtrl;
