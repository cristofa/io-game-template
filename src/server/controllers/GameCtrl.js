import express from 'express';
import jsdom from 'jsdom';

import path from 'path';

class GameCtrl {
  _controller = express.Router();

  constructor(io) {
    const Datauri = require('datauri');
    const datauri = new Datauri();

    const {JSDOM} = jsdom;

    let gamePath;
    if (process.env.NODE_ENV === 'development') {
      gamePath = path.resolve('./build-game/server_index.html');
    } else {
      gamePath = path.resolve('./game/server_index.html');
    }
    JSDOM.fromFile(gamePath, {
      runScripts: 'dangerously',
      resources: 'usable',
      pretendToBeVisual: true
    }).then((dom) => {
      dom.window.URL.createObjectURL = (blob) => {
        if (blob){
          return datauri.format(blob.type, blob[Object.getOwnPropertySymbols(blob)[0]]._buffer).content;
        }
      };
      // eslint-disable-next-line no-unused-vars
      dom.window.URL.revokeObjectURL = (objectURL) => {};
      dom.window.gameLoaded = () => {
        console.log('GAME LOADED!');
      };

      dom.window.io = io;
    }).catch((error) => {
      console.log(error.message);
    });
  }


  get controller() {
    return this._controller;
  }
}

export default GameCtrl;
