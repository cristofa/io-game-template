import express from 'express';

import AuthCtrl from './AuthCtrl';
import GameCtrl from './GameCtrl';

const setupRouter = (io) => {

  const router = express.Router();
  const authCtrl = new AuthCtrl();
  router.use('/auth', authCtrl.controller);
  const gameCtrl = new GameCtrl(io);
  router.use('/game', gameCtrl.controller);

  return router;
};

export default setupRouter;
