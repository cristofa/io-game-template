import  { Sequelize, DataTypes } from 'sequelize';

import userModel from './models/userModel';


const dbConnect = (config) => {
  return new Sequelize(`postgres://${config.dbUser}:${config.dbPass}@${config.dbHost}:${config.dbPort}/${config.dbName}`);
};

const dbInit = (sequelize) => {
  const Users = userModel(sequelize, DataTypes);
  return { Users };
};

export { dbConnect, dbInit };

