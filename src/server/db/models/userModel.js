const userModel = (sequelize, types) => {
  return sequelize.define('User', {
    userName: {
      type: types.STRING,
      allowNull: false
    }
  }, {
  });
};

export default userModel;
