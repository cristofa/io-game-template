import Phaser from 'phaser';

import Game from './scene/Game';

const config = {
  type: Phaser.HEADLESS,
  backgroundColor: '#ab2d5f',
  autoFocus: false,
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'game',
    width: 1280,
    height: 720,
  },
  physics: {
    default: 'matter',
    matter: {
      debug: false,
      gravity: { y: 0, x: 0 }
    }
  },
  scene: [Game]
};

new Phaser.Game(config);
