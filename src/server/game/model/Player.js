import Phaser from 'phaser';

class Player extends Phaser.Physics.Matter.Image {
  points = 0;

  constructor(scene, x, y) {
    super(scene.matter.world, x, y, 'img');
    this.setOrigin(0.5, 0.5).setDisplaySize(53, 40);
    this.setAngle(0);
    this.setFrictionAir(0.05);
    this.setMass(30);

    scene.add.existing(this);
  }

  addPoint() {
    this.points++;
  }

  getInfo() {
    return {
      x: this.x,
      y: this.y,
      rotation: this.rotation,
      points: this.points
    };
  }
}

export default Player;
