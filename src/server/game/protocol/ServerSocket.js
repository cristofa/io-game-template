import NET_MSGS from '../../../common/NET_MSGS';

class ServerSocket {
  constructor(emitter) {
    this.players = {};
    this.emitter = emitter;
    // eslint-disable-next-line no-undef
    this.socket = io;
  }

  sendUpdate() {
    this.socket.emit(NET_MSGS.PLAYER.UPDATE, this.players);
  }

  setPlayerInfo(id, info) {
    Object.assign(this.players[id], info);
  }

  init(handlers) {
    const players = this.players;
    const ioSocket = this.socket;
    // eslint-disable-next-line no-undef
    ioSocket.on('connection', (socket) => {
      console.log('a user connected');
      socket.emit(NET_MSGS.PLAYER.INIT, players);

      // create a new player and add it to our players object
      players[socket.id] = {
        rotation: 0,
        x: Math.floor(Math.random() * 700) + 50,
        y: Math.floor(Math.random() * 500) + 50,
        playerId: socket.id,
        points: 0,
        team: (Math.floor(Math.random() * 2) == 0) ? 'red' : 'blue',
        input: {
          left: false,
          right: false,
          up: false
        }
      };
      handlers[NET_MSGS.PLAYER.NEW](players[socket.id]);

      socket.broadcast.emit(NET_MSGS.PLAYER.NEW, players[socket.id]);
      socket.emit(NET_MSGS.PLAYER.NEW, players[socket.id]);

      socket.on('disconnect', () => {
        console.log('user disconnected');
        handlers[NET_MSGS.DISCONNECT](socket.id);
        delete players[socket.id];
        ioSocket.emit(NET_MSGS.DISCONNECT, socket.id);
      });

      socket.on(NET_MSGS.PLAYER.SET_NAME, (payload) => {
        console.log(payload);
      });

      socket.on(NET_MSGS.PLAYER.INPUT, (inputData) => {
        handlers[NET_MSGS.PLAYER.INPUT](socket.id, inputData);
      });
    });
  }
}


export default ServerSocket;
