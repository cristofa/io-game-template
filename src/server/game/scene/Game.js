import Phaser from 'phaser';

import ServerSocket from '../protocol/ServerSocket';
import Player from '../model/Player';
import NET_MSGS from '../../../common/NET_MSGS';


class Game extends Phaser.Scene {

  constructor() {
    super({key: 'Game'});
  }

  preload() {
    this.load.image('img', 'assets/images/player1.png');
    window.gameLoaded();
  }

  newPlayer(playerInfo) {
    const player = new Player(this, playerInfo.x, playerInfo.y);
    player.playerId = playerInfo.playerId;
    this.players.add(player);
  }

  removePlayer(playerId) {
    this.players.getChildren().forEach((player) => {
      if (playerId === player.playerId) {
        player.destroy();
      }
    });
  }

  playerInput (playerId, input) {
    this.players.getChildren().filter(p => playerId === p.playerId)[0].input = input;
  }

  createHandlers() {
    const handlers = [];
    handlers[NET_MSGS.PLAYER.NEW] = this.newPlayer.bind(this);
    handlers[NET_MSGS.DISCONNECT] = this.removePlayer.bind(this);
    handlers[NET_MSGS.PLAYER.INPUT] = this.playerInput.bind(this);
    return handlers;
  }

  create() {
    this.players = this.add.group();
    this.socket = new ServerSocket();
    this.socket.init(this.createHandlers());

    this.matter.world.setBounds(0, 0, 1280, 720);

  }

  update() {
    this.players.getChildren().forEach((player) => {
      const input = player.input;

      if (!input) {
        return;
      }
      if (input.left) {
        player.setAngle(player.angle - 10);
      } else if (input.right) {
        player.setAngle(player.angle + 10);
      }

      if (input.up) {
        player.thrust(0.1);
      }

      if (input.space) {
        player.addPoint();
      }
      this.socket.setPlayerInfo(player.playerId, player.getInfo());

    });
    this.socket.sendUpdate();
  }
}
export default Game;
