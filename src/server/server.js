import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import session from 'express-session';

import setupRouter from './controllers';
import config from './config';
import {dbConnect, dbInit} from './db/db';

const io = require('socket.io')();

const app = express();

app.use(session({
  secret: config.sessionSecret,
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('public'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());


app.use('/', setupRouter(io));

const dbConnection = dbConnect(config);
const { Users } = dbInit(dbConnection);


if (process.env.NODE_ENV === 'development') {
  app.use(express.static('build-game'));
  app.use(express.static('build-client'));
}


export {dbConnection, io};

export default app;
