const webpack = require('webpack');

const config = require('./webpack.config.base');
const findConfig = require('./utils');

let client = findConfig(config, 'client');

client = {
  ...client,
  output: {
    publicPath: '/',
  },
  plugins: [
    ...client.plugins,
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    host: '0.0.0.0',
    port: 3001,
    historyApiFallback: true,
    hot: true,
    contentBase: ['./public', './build-client']
  },
};

module.exports = client;
