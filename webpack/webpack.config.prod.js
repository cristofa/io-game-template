const webpack = require('webpack');
const dotenv = require('dotenv');

const path = require('path');
const config = require('./webpack.config.base');
const findConfig = require('./utils');


const env = dotenv.config().parsed;
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next]);
  return prev;
}, {});


const client = findConfig(config, 'client');
client.mode = 'production';
client.output = {
  path: path.join(__dirname, '../build/public'),
  publicPath: '/',
  filename: 'game.js',
};
client.plugins.push(new webpack.DefinePlugin(envKeys));

const server = findConfig(config, 'server');
server.mode = 'production';
server.plugins.push(new webpack.DefinePlugin(envKeys));

const game = findConfig(config, 'game');
game.devtool = false;
game.mode = 'production';
game.output = {
  path: path.join(__dirname, '../build/game'),
  filename: 'game.[name].min.js'
};

module.exports = [client, server, game];
